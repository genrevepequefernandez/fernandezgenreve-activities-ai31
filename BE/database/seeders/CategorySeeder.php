<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            ['category' => 'Romance'],
            ['category' => 'Fantasy'],
            ['category' => 'Non-Fiction'],
            ['category' => 'Mystery'],
            ['category' => 'Thriller'],
        ]; 

        foreach ($category as $categ) {
            Category::create($categ); 
        }
    }
}
