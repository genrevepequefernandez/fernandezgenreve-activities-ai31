<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Http\Requests\ReturnedBookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use function PHPUnit\Framework\isEmpty;

/**
 * @group Returned Book Controller
 * 
 */

class ReturnedBookController extends Controller
{

    /**
     * [GET] Show all records of returned books
     * 
     * Shows all returned book data from the database.
     * 
     * @response [
     *   {
     *       "id": 11,
     *       "book_id": 16,
     *       "copies": 3,
     *       "patron_id": 17,
     *       "created_at": "2021-01-06T09:57:59.000000Z",
     *       "updated_at": "2021-01-06T09:57:59.000000Z",
     *       "book": {
     *           "id": 16,
     *           "name": "After Midnight",
     *           "author": "Teresa Medeiros",
     *           "copies": 4,
     *           "category_id": 11,
     *           "created_at": "2021-01-04T16:36:17.000000Z",
     *           "updated_at": "2021-01-07T04:32:15.000000Z",
     *           "category": {
     *               "id": 11,
     *               "category": "Romance",
     *               "created_at": null,
     *               "updated_at": null
     *           }
     *       },
     *       "patron": {
     *           "id": 17,
     *           "last_name": "Edmunds",
     *           "first_name": "Phil",
     *           "middle_name": "A",
     *           "email": "philedmunds@yahoo.com",
     *           "created_at": "2021-01-05T14:32:03.000000Z",
     *           "updated_at": "2021-01-05T14:32:03.000000Z"
     *       }
     *   }
     * ]
     * 
     */ 


    public function index()
    {
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());
    }


    /**
     * [POST] Stores a record of returned book to database
     * 
     * Stores returned book into the database, update borrowed book and copies
     * 
     * @bodyParam book_id integer required Book id must exist in the borrowed book table. Example: 16
     * @bodyParam patron_id integer Must exist in the borrowed books table. Example: 17
     * @bodyParam copies integer required Must not exceed copies of book. Example: 1
     * 
     * @response 200 {
     *  "message": "Book returned successfully!"
     * }
     */  


    public function store(ReturnedBookRequest $request)
    {
    
        $borrowed = BorrowedBook::where([
            ['book_id', $request->book_id],
            ['patron_id', $request->patron_id],
        ])->firstOrFail();
        
        if(!empty($borrowed))
        {
            if($borrowed->copies == $request->copies){
                $borrowed->delete();
            }
            else
            {
                $borrowed->update(['copies' => $borrowed->copies - $request->copies]);
            }   
            
            $create_returned = ReturnedBook::create($request->only(['book_id', 'copies', 'patron_id']));
            $returned = ReturnedBook::with(['book'])->find($create_returned->id);
            $copies = $returned->book->copies + $request->copies;
            
            $returned->book->update(['copies' => $copies]);
            return response()->json(['message' => 'Book returned successfully!', 'book' => $returned]);
        }  

    }

    /**
     * [GET] Shows a record of returned book
     * 
     * Shows a returned book data by id 
     * 
     * @response 200 {
     *   "id": 12,
     *   "book_id": 16,
     *   "copies": 1,
     *   "patron_id": 17,
     *   "created_at": "2021-01-07T05:42:23.000000Z",
     *   "updated_at": "2021-01-07T05:42:23.000000Z",
     *   "book": {
     *       "id": 16,
     *       "name": "After Midnight",
     *       "author": "Teresa Medeiros",
     *       "copies": 5,
     *       "category_id": 11,
     *       "created_at": "2021-01-04T16:36:17.000000Z",
     *       "updated_at": "2021-01-07T05:42:23.000000Z",
     *       "category": {
     *           "id": 11,
     *           "category": "Romance",
     *           "created_at": null,
     *           "updated_at": null
     *       }
     *   },
     *   "patron": {
     *       "id": 17,
     *       "last_name": "Edmunds",
     *       "first_name": "Phil",
     *       "middle_name": "A",
     *       "email": "philedmunds@yahoo.com",
     *       "created_at": "2021-01-05T14:32:03.000000Z",
     *       "updated_at": "2021-01-05T14:32:03.000000Z"
     *   }
     * }
     * 
     * @response 404 {
     * "message": "Returned book not found"
     * }
     */

    public function show($id)
    {
        try {
            $returnedbook = ReturnedBook::with(['book', 'book.category', 'patron'])->findOrfail($id);
            return response()->json($returnedbook); 
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Returned book not found'], 404);
        }
    }
}
