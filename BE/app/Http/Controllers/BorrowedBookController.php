<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\BorrowedBook;
use App\Http\Requests\BorrowedBookRequest;

/**
 * @group Borrowed Book Controller
 */

class BorrowedBookController extends Controller
{

     /**
     * [GET] Show all borrowed book from the database
     * 
     * @response 200 [
     *   {
     *       "id": 20,
     *       "book_id": 16,
     *       "copies": 1,
     *       "patron_id": 17,
     *       "created_at": "2021-01-07T04:32:14.000000Z",
     *       "updated_at": "2021-01-07T04:32:14.000000Z",
     *       "patron": {
     *           "id": 17,
     *           "last_name": "Edmunds",
     *           "first_name": "Phil",
     *           "middle_name": "A",
     *           "email": "philedmunds@yahoo.com",
     *           "created_at": "2021-01-05T14:32:03.000000Z",
     *           "updated_at": "2021-01-05T14:32:03.000000Z"
     *       },
     *       "book": {
     *           "id": 16,
     *           "name": "After Midnight",
     *           "author": "Teresa Medeiros",
     *           "copies": 4,
     *           "category_id": 11,
     *           "created_at": "2021-01-04T16:36:17.000000Z",
     *           "updated_at": "2021-01-07T04:32:15.000000Z",
     *           "category": {
     *               "id": 11,
     *               "category": "Romance",
     *               "created_at": null,
     *               "updated_at": null
     *           }
     *       }
     *   },
     *   {
     *       "id": 21,
     *       "book_id": 17,
     *       "copies": 1,
     *       "patron_id": 18,
     *       "created_at": "2021-01-07T04:32:30.000000Z",
     *       "updated_at": "2021-01-07T04:32:30.000000Z",
     *       "patron": {
     *           "id": 18,
     *           "last_name": "Ince",
     *           "first_name": "Jessica",
     *           "middle_name": "B",
     *           "email": "jessicaince@yahoo.com",
     *           "created_at": "2021-01-05T14:32:25.000000Z",
     *           "updated_at": "2021-01-05T14:32:25.000000Z"
     *       },
     *       "book": {
     *           "id": 17,
     *           "name": "Moonlight Cove",
     *           "author": "Susan Donovan",
     *           "copies": 3,
     *           "category_id": 11,
     *           "created_at": "2021-01-05T05:49:03.000000Z",
     *           "updated_at": "2021-01-07T04:32:30.000000Z",
     *           "category": {
     *               "id": 11,
     *               "category": "Romance",
     *               "created_at": null,
     *               "updated_at": null
     *           }
     *       }
     *   }
     *]
     * 
     *
     */

    public function index(){
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
    }
    
    /**
     * [GET] Show specific data by id
     * 
     * @urlParam id required integer Refers to the book id. Example: 21
     * 
     * @response 200 {
     *   "id": 21,
     *   "book_id": 17,
     *   "copies": 1,
     *   "patron_id": 18,
     *   "created_at": "2021-01-07T04:32:30.000000Z",
     *   "updated_at": "2021-01-07T04:32:30.000000Z",
     *   "patron": {
     *       "id": 18,
     *       "last_name": "Ince",
     *       "first_name": "Jessica",
     *       "middle_name": "B",
     *       "email": "jessicaince@yahoo.com",
     *       "created_at": "2021-01-05T14:32:25.000000Z",
     *       "updated_at": "2021-01-05T14:32:25.000000Z"
     *   },
     *   "book": {
     *       "id": 17,
     *       "name": "Moonlight Cove",
     *       "author": "Susan Donovan",
     *       "copies": 3,
     *       "category_id": 11,
     *       "created_at": "2021-01-05T05:49:03.000000Z",
     *       "updated_at": "2021-01-07T04:32:30.000000Z",
     *       "category": {
     *           "id": 11,
     *           "category": "Romance",
     *           "created_at": null,
     *           "updated_at": null
     *       }
     *   }
     *}
     * 
     * 
     * @response 404 {
     * "message": "Borrowed book not found"
     * }
     */
    public function show($id)
    {
        try
        {
            $borrowedbook = BorrowedBook::with(['patron', 'book', 'book.category'])->where('id', $id)->firstOrFail();
            return response()->json($borrowedbook);
        } 
        catch (ModelNotFoundException $exception)
        {
            return response()->json(['message' => 'Borrowed book not found'], 404);
        }
    }

    /**
     * [POST] Stores Borrowed book.
     * 
     * @queryParam book_id integer required Book id must exist in the book table. Example: 25
     * @queryParam patron_id integer Must exist in the patron table. Example: 20
     * @queryParam copies integer required Must not exceed copies of book. Example: 1
     * 
     * @response 200 {
     *   "message": "Book borrowed successfully",
     *   "borrowedbook": {
     *       "id": 22,
     *       "book_id": 25,
     *       "copies": 1,
     *       "patron_id": 20,
     *       "created_at": "2021-01-07T04:40:24.000000Z",
     *       "updated_at": "2021-01-07T04:40:24.000000Z",
     *       "book": {
     *           "id": 25,
     *           "name": "Second Life",
     *           "author": "SJ Watson",
     *           "copies": 3,
     *           "category_id": 11,
     *           "created_at": "2021-01-05T14:29:38.000000Z",
     *           "updated_at": "2021-01-07T04:40:25.000000Z"
     *       }
     *   }
     *}
     */
    public function store(BorrowedBookRequest $request){
        $saveborrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));

        $borrowedbook = BorrowedBook::with(['book'])->find($saveborrowed->id);
        $copies = $borrowedbook->book->copies - $request->copies;
        $borrowedbook->book->update(['copies' => $copies]);

        return response()->json(['message' => 'Book borrowed successfully', 'borrowedbook' => $borrowedbook]);
    }
}
