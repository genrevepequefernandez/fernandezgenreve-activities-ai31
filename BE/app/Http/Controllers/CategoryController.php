<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * @group Categories Controller
 */

class CategoryController extends Controller
{
    /**
     * [GET] Shows all categories
     * 
     * 
     * @response 200 [
     *   {
     *       "id": 11,
     *       "category": "Romance",
     *       "created_at": null,
     *       "updated_at": null
     *   },
     *   {
     *       "id": 12,
     *       "category": "Fantasy",
     *       "created_at": null,
     *       "updated_at": null
     *   },
     *   {
     *       "id": 13,
     *       "category": "Non-Fiction",
     *       "created_at": null,
     *       "updated_at": null
     *   },
     *   {
     *       "id": 14,
     *       "category": "Mystery",
     *       "created_at": null,
     *       "updated_at": null
     *   },
     *   {
     *       "id": 15,
     *       "category": "Thriller",
     *       "created_at": null,
     *       "updated_at": null
     *   }
     *]
     * 
     */
    public function index(){
        $category = Category::all();
        return response()->json($category);
    }
}
