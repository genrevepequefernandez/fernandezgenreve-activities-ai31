<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Http\Requests\BookRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/** 
 * @group Book Controller
 */

class BookController extends Controller
{
    /**
     * [GET] Retrieving all book data stored in database
     * 
     * Displays all the books data from the database
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Book::with(['category:id,category'])->get());
    }

    /**
     * 
     * [POST] Stores book data into the database
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $stored = Book::create($request->validated());
		$book = Book::with(['category:id,category'])->find($stored->id);
        return response()->json(['message' => 'Book saved successfully!', 'book' => $book]);
    }

    /**
     * 
     * [GET] Retrieving book data by id
     * Display the specified resource.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            return response()->json($book);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Book not found'], 404);
        }
    }

    /**
     * [PUT] Update book data
     * Update the specified resource in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
        $book->update($request->validated());

        $updatedbook = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
        return response()->json(['message' => 'Book updated successfully!', 'book' => $updatedbook]);
    }

    /**
     * [DELETE] Deleting book data by id
     * Remove the specified resource from storage.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::where('id', $id)->firstOrFail();
        $book->delete();

        return response()->json(['message' => 'Book deleted successfully!']);
    }
}
