<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Patron; 
use App\Http\Requests\PatronRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * @group Patron Controller
 */

class PatronController extends Controller
{
    /**
     * 
     * [GET] Shows all patron from database
     * Display a listing of the resource.
     * 
     * @response 200 [
     *   {
     *       "id": 17,
     *       "last_name": "Edmunds",
     *       "first_name": "Phil",
     *       "middle_name": "A",
     *       "email": "philedmunds@yahoo.com",
     *       "created_at": "2021-01-05T14:32:03.000000Z",
     *       "updated_at": "2021-01-05T14:32:03.000000Z"
     *   },
     *   {
     *       "id": 18,
     *       "last_name": "Ince",
     *       "first_name": "Jessica",
     *       "middle_name": "B",
     *       "email": "jessicaince@yahoo.com",
     *       "created_at": "2021-01-05T14:32:25.000000Z",
     *       "updated_at": "2021-01-05T14:32:25.000000Z"
     *   },
     *   {
     *       "id": 20,
     *       "last_name": "Rees",
     *       "first_name": "Simon",
     *       "middle_name": "D",
     *       "email": "simonrees@gmail.com",
     *       "created_at": "2021-01-05T18:18:41.000000Z",
     *       "updated_at": "2021-01-05T18:18:41.000000Z"
     *   }
     *]
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patron = Patron::all();
        return response()->json($patron); 
    }

    /**
     * 
     * [POST] Add new patron to database
     * Show the form for creating a new resource.
     * 
     * @queryParam last_name required Last name of patron. Example: Fernandez
     * @queryParam middle_name required Middle name of patron. Example: Peque
     * @queryParam first_name required First name of patron. Example: Genreve
     * @queryParam email required Email address of patron. Must be unique. Example: genrevefernandez@gmail.com
     * 
     * @response 200 {
     *   "message": "Patron saved successfully!",
     *   "patron": {
     *       "last_name": "Fernandez",
     *       "first_name": "Genreve",
     *       "middle_name": "Peque",
     *       "email": "genrevefernandez@gmail.com",
     *       "updated_at": "2021-01-07T06:30:58.000000Z",
     *       "created_at": "2021-01-07T06:30:58.000000Z",
     *       "id": 39
     *   }
     *}
     *
     * @response 422 {
     *   "email": [
     *     "The email field is required."
     *   ]
     * }
     * 
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatronRequest $request)
    {
        $patron = Patron::create($request->validated());
        return response()->json(['message' => 'Patron saved successfully!', 'patron' => $patron]);
    }

    /**
     * [GET] Search patron by id
     * Display the specified resource.
     * 
     * @urlParam patron integer required Refers to the ID of the patron to be retrieved. Example: 39
     * 
     * @response 200 {
     *   "id": 39,
     *   "last_name": "Fernandez",
     *   "first_name": "Genreve",
     *   "middle_name": "Peque",
     *   "email": "genrevefernandez@gmail.com",
     *   "created_at": "2021-01-07T06:30:58.000000Z",
     *   "updated_at": "2021-01-07T06:30:58.000000Z"
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(Patron::findOrFail($id));
    }


    /**
     * 
     * [PUT] update Patron
     * Update the specified resource in storage.
     * 
     * @urlParam patron integer required ID of the patron to update. Example: 39
     * 
     * @queryParam last_name required Last name of patron. Example: Fernandez
     * @queryParam middle_name required Middle name of patron. Example: Peque
     * @queryParam first_name required First name of patron. Example: Genesis
     * @queryParam email required Email address of patron. Example: genesisfernandez@gmail.com 
     * 
     * @request 200 {
     *   "message": "Patron updated",
     *   "patron": {
     *       "id": 39,
     *       "last_name": "Fernandez",
     *       "first_name": "Genesis",
     *       "middle_name": "Peque",
     *       "email": "genesisfernandez@gmail.com",
     *       "created_at": "2021-01-07T06:30:58.000000Z",
     *       "updated_at": "2021-01-07T06:37:03.000000Z"
     *   }
     *} 
     *
     * @response 404 {
     *  "message": "Patron not found"
     * }
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatronRequest $request, $id)
    {
        try {
            $patron = Patron::findOrFail($id);
            $patron->update($request->validated());

            return response()->json(['message' => 'Patron updated', 'patron' => $patron]);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Patron not found'], 404);
        }
    }

    /**
     * 
     * [DELETE] Delete patron 
     * Remove the specified resource from storage.
     * 
     * @urlParam patron required integer Refers to the id of the patron.
     * 
     * @response 200 {
     *  "message": "Patron deleted successfully!
     * }
     * 
     * @response 404 {
     *  "message": "Patron not found"
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $patron = Patron::where('id', $id)->firstOrFail();
            $patron->delete();

            return response()->json(['message' => 'Patron deleted successfully!']);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['message' => 'Patron not found'], 404);
        } 
    }
}
