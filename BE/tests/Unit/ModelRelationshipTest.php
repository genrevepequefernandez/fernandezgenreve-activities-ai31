<?php

namespace Tests\Unit;
use App\Models\Category;
use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;
use App\Models\Patron; 
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ModelRelationshipTest extends TestCase
{

    use RefreshDatabase; 
    /**
     * A basic unit test example.
     *
     * @return void
     */
    
    public function test_book_has_a_category(){
        $category = Category::create(['category' => 'Novel']); 
        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);  
        $this->assertInstanceOf(Category::class, $book->category); 
    }


    public function test_book_has_many_borrowed(){
        $category = Category::create(['category' => 'Novel']); 
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);
        $borrowedbook = BorrowedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $book->borrowed);
    }

    public function test_book_has_many_returned(){
        $category = Category::create(['category' => 'Novel']); 
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);
        $returnedBook = ReturnedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $book->returned);
    }

    public function test_category_belongs_to_many_books(){
        $category = Category::create(['category' => 'Novel']);
        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $category->books); 
    }

    public function test_patron_has_many_returnedbooks(){
        $category = Category::create(['category' => 'Novel']);
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);

        $returnedBook = ReturnedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $patron->returnedbooks);
    }

    public function test_patron_has_many_borrowedbooks(){
        $category = Category::create(['category' => 'Novel']);
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);

        $borrowedBook = BorrowedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $patron->borrowedbooks);
    }

    public function test_borrowedbook_belongs_to_patron(){
        $category = Category::create(['category' => 'Novel']);
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);

        $borrowedBook = BorrowedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf(Patron::class, $borrowedBook->patron);
    }

    public function test_borrowedbook_belongs_to_book(){
        $category = Category::create(['category' => 'Novel']);
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);

        $borrowedBook = BorrowedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf(Book::class, $borrowedBook->book);
    }

    public function test_returnedbook_belongs_to_patron(){
        $category = Category::create(['category' => 'Novel']);
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);

        $returnedBook = ReturnedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf(Patron::class, $returnedBook->patron);
    }

    public function test_returnedbook_belongs_to_book(){
        $category = Category::create(['category' => 'Novel']);
        $patron = Patron::create([
            'last_name' => 'Fernandez',
            'first_name' => 'Genreve',
            'middle_name' => 'Peque', 
            'email' => 'genrevepequefernandez@gmail.com'
        ]);

        $book = Book::create([
            'name' => 'Last Chance Rebel', 
            'author' => 'Maisey Yates',
            'category_id' => $category->id,
            'copies' => rand(1, 2000) 
        ]);

        $returnedBook = ReturnedBook::create([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id 
        ]);

        $this->assertInstanceOf(Book::class, $returnedBook->book);
    }
}
