# Returned Book Controller


## [GET] Show all records of returned books


Shows all returned book data from the database.

> Example request:

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://127.0.0.1:8000/api/returnedbook',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
[
    {
        "id": 11,
        "book_id": 16,
        "copies": 3,
        "patron_id": 17,
        "created_at": "2021-01-06T09:57:59.000000Z",
        "updated_at": "2021-01-06T09:57:59.000000Z",
        "book": {
            "id": 16,
            "name": "After Midnight",
            "author": "Teresa Medeiros",
            "copies": 4,
            "category_id": 11,
            "created_at": "2021-01-04T16:36:17.000000Z",
            "updated_at": "2021-01-07T04:32:15.000000Z",
            "category": {
                "id": 11,
                "category": "Romance",
                "created_at": null,
                "updated_at": null
            }
        },
        "patron": {
            "id": 17,
            "last_name": "Edmunds",
            "first_name": "Phil",
            "middle_name": "A",
            "email": "philedmunds@yahoo.com",
            "created_at": "2021-01-05T14:32:03.000000Z",
            "updated_at": "2021-01-05T14:32:03.000000Z"
        }
    }
]
```
<div id="execution-results-GETapi-returnedbook" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-returnedbook"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-returnedbook"></code></pre>
</div>
<div id="execution-error-GETapi-returnedbook" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-returnedbook"></code></pre>
</div>
<form id="form-GETapi-returnedbook" data-method="GET" data-path="api/returnedbook" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-returnedbook', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-returnedbook" onclick="tryItOut('GETapi-returnedbook');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-returnedbook" onclick="cancelTryOut('GETapi-returnedbook');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-returnedbook" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/returnedbook</code></b>
</p>
</form>


## [POST] Stores a record of returned book to database


Stores returned book into the database, update borrowed book and copies

> Example request:

```php

$client = new \GuzzleHttp\Client();
$response = $client->post(
    'http://127.0.0.1:8000/api/returnedbook',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
        'json' => [
            'book_id' => 16,
            'copies' => 1,
            'patron_id' => 17,
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "message": "Book returned successfully!"
}
```
<div id="execution-results-POSTapi-returnedbook" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-returnedbook"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-returnedbook"></code></pre>
</div>
<div id="execution-error-POSTapi-returnedbook" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-returnedbook"></code></pre>
</div>
<form id="form-POSTapi-returnedbook" data-method="POST" data-path="api/returnedbook" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-returnedbook', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-returnedbook" onclick="tryItOut('POSTapi-returnedbook');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-returnedbook" onclick="cancelTryOut('POSTapi-returnedbook');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-returnedbook" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/returnedbook</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>book_id</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="book_id" data-endpoint="POSTapi-returnedbook" data-component="body" required  hidden>
<br>
Book id must exist in the borrowed book table.</p>
<p>
<b><code>copies</code></b>&nbsp;&nbsp;<small>integer</small>  &nbsp;
<input type="number" name="copies" data-endpoint="POSTapi-returnedbook" data-component="body" required  hidden>
<br>
Must not exceed copies of book.</p>
<p>
<b><code>patron_id</code></b>&nbsp;&nbsp;<small>integer</small>     <i>optional</i> &nbsp;
<input type="number" name="patron_id" data-endpoint="POSTapi-returnedbook" data-component="body"  hidden>
<br>
Must exist in the borrowed books table.</p>

</form>


## [GET] Shows a record of returned book


Shows a returned book data by id

> Example request:

```php

$client = new \GuzzleHttp\Client();
$response = $client->get(
    'http://127.0.0.1:8000/api/returnedbook/quo',
    [
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ],
    ]
);
$body = $response->getBody();
print_r(json_decode((string) $body));
```


> Example response (200):

```json
{
    "id": 12,
    "book_id": 16,
    "copies": 1,
    "patron_id": 17,
    "created_at": "2021-01-07T05:42:23.000000Z",
    "updated_at": "2021-01-07T05:42:23.000000Z",
    "book": {
        "id": 16,
        "name": "After Midnight",
        "author": "Teresa Medeiros",
        "copies": 5,
        "category_id": 11,
        "created_at": "2021-01-04T16:36:17.000000Z",
        "updated_at": "2021-01-07T05:42:23.000000Z",
        "category": {
            "id": 11,
            "category": "Romance",
            "created_at": null,
            "updated_at": null
        }
    },
    "patron": {
        "id": 17,
        "last_name": "Edmunds",
        "first_name": "Phil",
        "middle_name": "A",
        "email": "philedmunds@yahoo.com",
        "created_at": "2021-01-05T14:32:03.000000Z",
        "updated_at": "2021-01-05T14:32:03.000000Z"
    }
}
```
> Example response (404):

```json
{
    "message": "Returned book not found"
}
```
<div id="execution-results-GETapi-returnedbook--returnedbook-" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-returnedbook--returnedbook-"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-returnedbook--returnedbook-"></code></pre>
</div>
<div id="execution-error-GETapi-returnedbook--returnedbook-" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-returnedbook--returnedbook-"></code></pre>
</div>
<form id="form-GETapi-returnedbook--returnedbook-" data-method="GET" data-path="api/returnedbook/{returnedbook}" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-returnedbook--returnedbook-', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-returnedbook--returnedbook-" onclick="tryItOut('GETapi-returnedbook--returnedbook-');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-returnedbook--returnedbook-" onclick="cancelTryOut('GETapi-returnedbook--returnedbook-');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-returnedbook--returnedbook-" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/returnedbook/{returnedbook}</code></b>
</p>
<h4 class="fancy-heading-panel"><b>URL Parameters</b></h4>
<p>
<b><code>returnedbook</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="returnedbook" data-endpoint="GETapi-returnedbook--returnedbook-" data-component="url" required  hidden>
<br>
</p>
</form>



