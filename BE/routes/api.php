<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\PatronController; 
Use App\Http\Controllers\BookController; 
Use App\Http\Controllers\CategoryController; 
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\ReturnedBookController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('patrons', PatronController::class)->only(['index', 'store', 'show', 'update', 'destroy']);
Route::resource('books', BookController::class)->only(['index', 'store', 'show', 'update', 'destroy']);
Route::resource('/borrowedbook', BorrowedBookController::class);
Route::resource('/returnedbook', ReturnedBookController::class);
Route::get('/categories', [CategoryController::class, 'index']);

