var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Jan', 'Feb', 'April', 'May', 'June', 'July'], 
        datasets: [{
            label: 'Borrowed Books', 
            data: [10, 18, 10, 65, 50, 71, 23], 
            backgroundColor: [
                'rgba(152, 102, 255, 5)',
                'rgba(152, 102, 255, 5)',
                'rgba(152, 102, 255, 5)',
                'rgba(152, 102, 255, 5)',
                'rgba(152, 102, 255, 5)',
                'rgba(152, 102, 255, 5)'
            ],
        },
    
        {
            label: 'Returned Books', 
            data: [40, 5, 4, 98, 2, 13, 40], 
            backgroundColor: [
                'rgba(204, 0, 204, 5)',
                'rgba(204, 0, 204, 5)',
                'rgba(204, 0, 204, 5)',
                'rgba(204, 0, 204, 5)',
                'rgba(204, 0, 204, 5)',
                'rgba(204, 0, 204, 5)'
            ],
        }]
    },

    options: {
        legend: {
            labels: {
                 fontColor: 'white'
                }
             },

        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    fontColor: 'white'
                }
            }],

            xAxes: [{
                ticks: {
                    fontColor: 'white'
                },
            }]
        }
    }
});

var pie = document.getElementById('myDoughnutChart').getContext('2d');
var pieChart = new Chart(pie, {
  type: 'doughnut',
  data: {
    datasets: [{
        data: [45, 5, 2, 43],
        backgroundColor: [
          'rgb(152, 102, 255, 5 )',
          'rgba(204, 102, 255, 5)',
          'rgba(204, 0, 255, 5)',
          'rgba(128, 0, 128, 5)',
        ],
        borderWidth: [0, 0, 0, 0]
    }],

    labels: [
        'Romance',
        'Fantasy',
        'Non-Fiction',
        'Mystery'
    ]
  },
  options: {
    legend: {
        labels: {
             fontColor: 'white'
            }
         },

    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true,
                fontColor: 'white'
            }
        }]
    }
}
});


