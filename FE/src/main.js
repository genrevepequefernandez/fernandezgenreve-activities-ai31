import Vue from 'vue'
import { BootstrapVue, IconsPlugin} from 'bootstrap-vue'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

Vue.config.productionTip = false

import './assets/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/style.css'
import router from './router'
import App from './App.vue'
import store from './store'
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true
});

Vue.config.productionTip = false

new Vue({
  router, store, 
  render: function (h) { return h(App) }
}).$mount('#app')